const express = require('express');
const usuarioCtrl= express.Router();
const Usuario = require ("../models/Usuario");

//funcion 1 listar usuario1
 usuarioCtrl.get("/",(req,res)=>{
    Usuario.find()
    .then(data=>{
        res.json(data);
    })
    .catch(err=>{
        res.json({message:err})});
    }); 

//funcion2 listar usuarios
   /*  usuarioCtrl.get("/",async()=>{
       let usuario = await Usuario.find()
       res.json(usuario);
        }); 
         */
    //BUSCRA POR ID

   /*  usuarioCtrl.get("/:id",async(req,res)=>{
        let usuario = await Usuario.findById({_id: req.params.id})
        res.json(usuario);
         }); */ 

         //consultar por id
         usuarioCtrl.get("/id/:id",async (req,res)=>{
            let usuario = await Usuario.findById( req.params.id);
            res.json(usuario);

         });

//buscar login
usuarioCtrl.get("/consultaUsuario", async (req,res) =>{
    let usuario = await Usuario.findOne({usuario: req.body.usuario, clave: req.body.clave})
    res.json(usuario);
})

         //Crear usuario metodo post
        usuarioCtrl.post("/",(req,res)=>{
            const usuario =new Usuario(req.body);
            usuario.save()
            .then((data)=>{
                res.json(data);
            })
            .catch((err)=>{
                res.json({message:err});

            });
        }); 
          
            
/* //otra funcion post
   usuarioCtrl.post("/",async(req,res)=>{
      let usuario = req.body;
      usuario =await Usuario.create(usuario)
      res.json("usuario creado");
     });
           */
             
          //actualizar usuario
             
                 
                  
     usuarioCtrl.patch("/",async(req,res)=>{
        let usuario = req.body;
        usuario = await Usuario.updateOne({_id:usuario._id}, usuario) ;
        res.json(usuario);
        });  
    //actualizar usuario


     /* usuarioCtrl.patch("/:_id",(req,res)=>{
        Usuario.updateOne({_id: req.params.id},{
            $set:{
                usuario:req.body.usuario,
                nombres:req.body.nombres,
                apellidos:req.body.apellidos,
                tipoDoc:req.body.tipDoc,
                numDoc:req.body.numDoc,
                direccion:req.body.direccion,
                telefono:req.body.telefono,
                email:req.body.email,
                estado:req.body.email,
    
        }
        }
        )
        .then((data)=> {
            res.json(data);   
        })
         .catch((err)=>{
            res.json({message:err});
        });
        
    }); 
     */

/* 
         usuarioCtrl.delete("/:_id",(req,res)=>{
            Usuario.deleteOne({_id: req.params.id})
            .then((data)=> {
                res.json(data);   
            })
             .catch((err)=>{
                res.json({message:err});
            });
            }); */

usuarioCtrl.delete("/:_id", async (req,res)=>{
    await Usuario.deleteOne({_id: req.params.id});
    res.send("usuario eliminado")
})            


         module.exports=usuarioCtrl;

        
        

